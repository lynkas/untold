import React, {useEffect, useState} from "react";
import MediaPage from "./component/MediaPage";
import Main from "./component/Main";
import axios from 'axios'
import {find, inList} from "./component/utils/utils";
import {findDOMNode} from "react-dom";
// import Make from "./component/Make";
// import Portable from "./component/Portable";
import Container from "@material-ui/core/Container";
import Header from "./component/Header";
import "./css/color.scss"
import Footer from "./component/Footer";
import {
    ADDEDTIME,
    INTRODUCTION,
    NAME,
    PERSONALLIST,
    SUBLINK,
    SUBSCRIBELIST,
    TITLE, LINKS, VIDEO, LINK,
    VIDEOLIST, UPDATE
} from "./component/utils/names";
import {Route, Router, Switch} from "react-router";
// import {openDB} from "./component/utils/db";
import {getDB,setDB} from "./component/db/db"
import history from "./component/utils/history";
import Subscribe from "./component/Subscribe";
import SubscribeList from "./component/SubscribeList";
import {openDB} from "idb";
import Make from "./component/Make";
export default function App(props) {
    const [ok,setOK]=useState(false)


    useEffect(()=>{
        databaseInit()
    },[])

    const databaseInit = async () => {

        const db = await openDB(VIDEO,1,{
            upgrade(database, oldVersion, newVersion, transaction) {
                let db = database;
                console.log("upgrade")
                let objectStore;
                if (!db.objectStoreNames.contains(VIDEOLIST)) {
                    objectStore = db.createObjectStore(VIDEOLIST, {keyPath:LINK});
                    objectStore.createIndex(TITLE, TITLE, { unique: false });
                    objectStore.createIndex(LINK, LINK, { unique: true });
                    objectStore.createIndex(ADDEDTIME, ADDEDTIME, { unique: false });
                    objectStore.createIndex(SUBLINK, SUBLINK, { unique: false });
                }
                if (!db.objectStoreNames.contains(SUBSCRIBELIST)) {
                    objectStore = db.createObjectStore(SUBSCRIBELIST, {keyPath:LINK});
                    objectStore.createIndex(LINK, LINK, { unique: true });
                    objectStore.createIndex(NAME, NAME, { unique: false });
                    objectStore.createIndex(INTRODUCTION, INTRODUCTION, { unique: false });
                    objectStore.createIndex(UPDATE, UPDATE, { unique: false });
                }
                if (!db.objectStoreNames.contains(PERSONALLIST)) {
                    objectStore = db.createObjectStore(PERSONALLIST, { autoIncrement:true });
                    objectStore.createIndex(NAME, NAME, { unique: false });
                    objectStore.createIndex(LINKS, LINKS, { unique: false });
                }
            }
        })
        setOK(true)
        setDB(db)

        // let request = window.indexedDB.open(VIDEO);
        // request.onerror = function (event) {
        //     console.log('error when opening indexeddb');
        // };
        //
        // request.onsuccess = function (event) {
        //     setDB(request.result);
        //     console.log('db opened');
        //     setOK(true)
        // };
        // request.onupgradeneeded = function (event) {
        //     let db = event.target.result;
        //     console.log("upgrade")
        //     let objectStore;
        //     if (!db.objectStoreNames.contains(VIDEOLIST)) {
        //         objectStore = db.createObjectStore(VIDEOLIST, {keyPath:LINK});
        //         objectStore.createIndex(TITLE, TITLE, { unique: false });
        //         objectStore.createIndex(LINK, LINK, { unique: true });
        //         objectStore.createIndex(ADDEDTIME, ADDEDTIME, { unique: false });
        //         objectStore.createIndex(SUBLINK, SUBLINK, { unique: false });
        //     }
        //     if (!db.objectStoreNames.contains(SUBSCRIBELIST)) {
        //         objectStore = db.createObjectStore(SUBSCRIBELIST, {keyPath:LINK});
        //         objectStore.createIndex(LINK, LINK, { unique: true });
        //         objectStore.createIndex(NAME, NAME, { unique: false });
        //         objectStore.createIndex(INTRODUCTION, INTRODUCTION, { unique: false });
        //         objectStore.createIndex(UPDATE, UPDATE, { unique: false });
        //     }
        //     if (!db.objectStoreNames.contains(PERSONALLIST)) {
        //         objectStore = db.createObjectStore(PERSONALLIST, { autoIncrement:true });
        //         objectStore.createIndex(NAME, NAME, { unique: false });
        //         objectStore.createIndex(LINKS, LINKS, { unique: false });
        //     }
        // }
    }


    return <React.Fragment>

        {ok?<Container maxWidth={"xl"}>
            <Router history={history}>
                <Header />
                <Switch>
                    <Route path={"/vid"}>
                        <MediaPage/>
                    </Route>
                    <Route exact path={"/sub"}>
                        <SubscribeList />
                    </Route>
                    <Route path={"/sub"}>
                        <Subscribe />
                    </Route>
                    <Route exact path={"/set/make"}>
                        <Make />
                    </Route>
                        {/*<Route path={"/set"}>*/}
                        {/*    <Subscribe />*/}
                        {/*</Route>*/}
                    <Route>
                        <Main
                            // subscriptionList={subscribeLinks}
                            // videoList={videoCache}
                            // setVideoList={setVideoCache}
                            // removeSubscription = {removeSubscription}
                            // addSubscription = {addSubscription}
                        />
                    </Route>
                </Switch>
            </Router>
        </Container>:"db not ok"}


        <Footer />
    </React.Fragment>

}
