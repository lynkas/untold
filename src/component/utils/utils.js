
import axios from "axios";
import {addVideoToDB} from "./db";
import {getDB} from "../db/db";
import {SUBSCRIBELIST} from "./names";
// export function inList(link,list) {
//     for (const i in list){
//         if (list[i].url===link)return list[i]
//     }
//     return false
// }
//
//
// export function find(link,list) {
//     for (const i in list){
//         if (list[i].url===link)return parseInt(i)
//     }
//     return -1
// }
//

export const download = (filename,plain)=> () => {
    let element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(plain));
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}
//
// export const load = (callback)=> {
//     let element = document.createElement('input');
//     element.setAttribute('type', 'file');
//     element.addEventListener("change",(e)=>{
//         const file = element.files[0];
//         let reader = new FileReader();
//         reader.onload = function(e) {
//             const content = reader.result;
//             callback(JSON.parse(content))
//         }
//         reader.readAsText(file);
//
//     })
//     element.style.display = 'none';
//     document.body.appendChild(element);
//     element.click();
//     document.body.removeChild(element);
// }
//

export const getLink = location =>{
    const pathSeg = location.pathname.split("/")
    if (pathSeg.length<=2){
        return null
    }else if (pathSeg.length===3&&pathSeg[2]===""){
        return null
    }else {
        const links = pathSeg.slice(2)
        return links.join("/")
    }
}

export function validURL(str) {
    const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

export const originalGet = async (link) =>{
    const response = await axios.get(link,{transformResponse: (response) =>{return response},timeout:5000})
    return response.data
}

export const one = (o) => {
    if (o==null)return null
    return !(Object.keys(o).length === 0 && o.constructor === Object)
}

export const currentSeries = (o)=>{
    if (!one(o))return{}

    let part = 0
    if (o.hasOwnProperty("part")){
        part=o.part
    }
    if (!o.hasOwnProperty("series"))return {}
    if (!o.series)return {}
    if (!o.series.length)return {}
    if (o.series.length<part)return {}
    return o.series[part]
}

export const slHandle = content => {
    const lines = content.split(/\r?\n/)
    let introduction = ""
    const links = []
    for (const line of lines){
        if (!line.trim())continue
        if (line.trim().startsWith("#")){
            introduction=introduction.concat(line,"\r\n")
        }else {
            links.push(line.trim())
        }
    }
    if (introduction.endsWith("\r\n"))introduction=introduction.substring(0,introduction.length-2)
    return [introduction,links]
}



export const getAndSaveVideo = async (url,from) => {
    try{
        const data = await getOneVideo(url)
        await addVideoToDB(data,url,from)
    }catch (e) {
        console.log(e)
    }
}



export async function getOneVideo(link) {
        const plain = await originalGet(link)
        const data = JSON.parse(plain)
        return data
}

export async function getOneSubscribe(link){
        const data = await originalGet(link)
        return slHandle(data)
}

