import {db, getDB} from "../db/db";
import {LINK, SUBLINK, SUBSCRIBELIST, VIDEOLIST} from "./names";

const defaultVideo={
    title:"",
    introduction:"",
    cover:"",
    series:[],
    part:0,
    time:0,
    hide:false
}

export const addVideoToDB =async (data,link,from=null,loaded=true) => {
    return getDB().transaction([VIDEOLIST], 'readwrite').objectStore(VIDEOLIST).put({
        link: link,
        subLink: from,
        loaded,
        addedTime: Date.now(),
        ...defaultVideo,
        ...data,
    });
}

export const addOneVideoToDB=async (data,link,from=null,loaded=true) => {
    const store = getDB().transaction(VIDEOLIST,'readwrite').objectStore(VIDEOLIST)
    return store.put({
        ...defaultVideo,
        ...data,
        addedTime: Date.now(),
        link: link,
        subLink: from,
        loaded
    });
}

export const addOneSubscribeToDB=async (link,introduction,update=true)=>{
    return getDB().transaction(SUBSCRIBELIST, 'readwrite').objectStore(SUBSCRIBELIST).put({link, introduction, update,LUT:Date.now()});
}

export const getVideoLinkFromDB = async (link) =>{
    return getDB().transaction(VIDEOLIST, 'readonly').objectStore(VIDEOLIST).index(LINK).get(link);
}

export const getSubscribeLinkFromDB = async (link) =>{
    return getDB().transaction(SUBSCRIBELIST, 'readonly').objectStore(SUBSCRIBELIST).index(LINK).get(link);
}

export const getSubscribeCursor = async() =>{
    return getDB().transaction(SUBSCRIBELIST, 'readonly').objectStore(SUBSCRIBELIST).openCursor(null,'prev')
}

export const getVideoCursor = async() =>{
    return getDB().transaction(VIDEOLIST, 'readonly').objectStore(VIDEOLIST).openCursor(null,'prev')
}

export const getVideoBySubscribeLink = async link => {
    return getDB().transaction([VIDEOLIST],"readonly").objectStore(VIDEOLIST).index(SUBLINK).getAll(link)
}

export const deleteSubscribeFromDB = async link => {
    return getDB().transaction([SUBSCRIBELIST],"readwrite").objectStore(SUBSCRIBELIST).delete(link)
}

// export async function addSubscribe(link,introduction,update=true) {
//     getDB().transaction([SUBSCRIBELIST],'readwrite').objectStore(SUBSCRIBELIST).put({link,introduction,update})
// }
//
//
// export const addVideoToDBNoReplace =async (data,link,from=null,loaded=true) => {
//     const request=  getDB().transaction([VIDEOLIST],'readonly').objectStore(VIDEOLIST).get(link)
//     request.onsuccess=e=>{
//         if (!request.result) addVideoToDB(data,link,from,loaded)
//     }
//
// }
//
// export const videoLinkProcess = async (link,from,store)=>{
//
//     let request = await store.index(LINK).get(link)
//     const o = await new Promise((resolve,reject)=>{
//         request.onsuccess=e=>resolve(e.result)
//         request.onerror=e=>reject(e)
//     })
//     if (!o){
//         try {
//             const result = await originalGet(link)
//             const obj = JSON.parse(result)
//             await addVideoToDB(obj,link,store,from)
//         }catch (e) {
//             console.log(e)
//         }
//
//     }
// }

