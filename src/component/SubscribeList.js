import React, {useEffect, useState} from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import PropTypes, {func} from 'prop-types';
import SubscribeListData from "./SubscribeListData";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {ArrowRight, ArrowRightAlt, Done, Error} from "@material-ui/icons";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useTranslation } from 'react-i18next';
import {getDB} from "./db/db";
import {SUBSCRIBELIST} from "./utils/names";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import {validURL} from "./utils/utils";
import Container from "@material-ui/core/Container";
import CardContent from "@material-ui/core/CardContent";
import {getSubscribeCursor} from "./utils/db";

export default function SubscribeList(props) {

    const [subscribeList,setSubscribeList] = useState([])

    const [last,setLast] = useState(0)
    const [subLink,setSubLink] = useState("")
    useEffect(()=>{
        (async ()=>{
            try{
                let  cursor = await getSubscribeCursor()
                if (cursor)await cursor.advance(last)
                let count = 0
                let last_temp = last

                const all = []
                while (cursor){
                    all.push(cursor.value)
                    last_temp++
                    if (count++<=25)cursor.continue()
                    else setSubscribeList(all)
                }
                setLast(last_temp)
                setSubscribeList(all)
            }catch (e) {
                console.log(e)
            }
        })()


    },[])

    const go = ()=>{
        return <Tooltip title={"go"}>
            <IconButton
                disabled={!subLink||!validURL(subLink)}
                onClick={()=>{
                window.location="/sub/"+subLink
            }} >
                <ArrowRightAlt />
            </IconButton>
        </Tooltip>
    }

    return <Container maxWidth={"md"}>

        <TextField
            fullWidth
            variant={"filled"}
            placeholder={"add a subscribe link"}
            value={subLink}
            error={subLink&&!validURL(subLink)}
            onChange={e=>{setSubLink(e.target.value)}}
            InputProps={{endAdornment:go()}}
        />
        {console.log(subscribeList)}
        {subscribeList?(subscribeList.map((e,i)=>{
        return <Card>
        <CardActionArea onClick={()=>{window.location="/sub/"+e.link}}>
            <CardContent>
                <Typography variant={"h4"} style={{position:"absolute",right:"0",top:"0",color:"gray"}}>{last-25+i}</Typography>
                <Typography variant={"subtitle1"}>{e.link}</Typography>
                <Typography variant={"subtitle1"}>{e.introduction}</Typography>
            </CardContent>

        </CardActionArea>
        </Card>
    })):null}
    </Container>

//     const { t, i18n } = useTranslation();
//
//     const [url,setUrl] = useState("")
//     const [name,setName] = useState("")
//
//     const update = fn => e => {
//         fn(e.target.value)
//     }
//
//     const save = ()=>{
//         props.resetTest()
//         props.addSubscription(url.trim(),name.trim())
//         setName("")
//         setUrl("")
//     }
//
//     const remove = id => e=>{
//         props.resetTest()
//         props.removeSubscription(id)
//     }
//
//     return <ExpansionPanel>
//         <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
//             <Typography>{t("subscription")}{t("list")} </Typography>
//         </ExpansionPanelSummary>
//         <ExpansionPanelDetails style={{display:"block"}}>
//             <div>
//                 <ButtonGroup>
//                     <TextField id="filled-basic" label={t("link")} variant="filled" onChange={update(setUrl)}/>
//                     <TextField id="filled-basic" label={t("name")} variant="filled" onChange={update(setName)}/>
//                     <Button variant={"outlined"} disabled={url.trim()===""} onClick={save} size={"large"} >
//                         {t("save")}
//                     </Button>
//                 </ButtonGroup>
//
//             </div>
//             {props.subscription&&props.subscription.length?
//                 <div style={{width:"100%",overflow:"auto"}}>
//                 <Table size={"small"}>
//                     <TableHead>
//                         <TableRow>
//                             <TableCell/>
//                             <TableCell>
//                                 <Typography>
//                                     {t("name")}
//                                 </Typography>
//                             </TableCell>
//                             <TableCell>
//                                 <Typography>
//                                     {t("link")}
//                                 </Typography>
//                             </TableCell>
//                             <TableCell>
//                                 <Typography>
//                                     {t("status")}
//                                 </Typography>
//                             </TableCell>
//                             <TableCell/>
//                             <TableCell/>
//                         </TableRow>
//                     </TableHead>
//                     <TableBody>
//                         {props.subscription.map((e,i)=>{
//                             return <TableRow key={i}>
//                                 <TableCell>
//                                     <Typography variant={"subtitle1"}>
//                                         {i}
//                                     </Typography>
//                                 </TableCell>
//                                 <TableCell>
//                                     <Typography variant={"subtitle1"}>
//                                         {e.name.trim()===""?<i>(blank)</i>:e.name.trim()}
//                                     </Typography>
//                                 </TableCell>
//                                 <TableCell>
//                                     <Typography variant={"subtitle1"}>
//                                         {e.link.trim()===""?<i>(blank)</i>:e.link.trim()}
//                                     </Typography>
//                                 </TableCell>
//                                 <TableCell>
//                                     {props.subscriptionUpdateState[i]==null?null:(props.subscriptionUpdateState[i]?<Done/>:<Error/>)}
//                                 </TableCell>
//                                 <TableCell>
//                                     <Tooltip title="Delete">
//                                         <IconButton aria-label="delete" onClick={remove(i)}>
//                                             <DeleteIcon />
//                                         </IconButton>
//                                     </Tooltip>
//                                 </TableCell>
//                             </TableRow>
//                         })}
//                     </TableBody>
//                 </Table>
//             </div>:null}
//         </ExpansionPanelDetails>
//     </ExpansionPanel>
// }


// SubscribeList.prototype={
//     subscription: PropTypes.arrayOf(SubscribeListData),
//     subscriptionUpdateState: PropTypes.array,
//     removeSubscription: PropTypes.func,
//     addSubscription: PropTypes.func,
//     resetTest: PropTypes.func
}
