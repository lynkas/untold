import React from "react";
import Video from "./Video";
import Audio from "./Audio";
import Figure from "./Figure";
import ETLink from "./Text";
import Text from "./Text";

export default function Media(props) {

    const medias = (props)=>{
        const response = []
        if (props.video)response.push(<Video video={props.video} subtitle={props.subtitle} time={props.time} setTime={props.setTime}/>)
        if (props.music)response.push(<Audio music={props.music}  time={props.time}/>)
        if (props.image)response.push(<Figure image={props.image}/>)
        if (props.text) response.splice(1,0,<Text text={props.text}/>)
        return response
    }

    return <React.Fragment>
        {medias(props)}
        {/*{console.log(props)}*/}
        {/*{props.video?<Video video={props.video} subtitle={props.subtitle} time={props.time}/>:null}*/}
        {/*{props.music?<Audio music={props.music}  time={props.time}/>:null}*/}
        {/*{props.text?<Text text={props.text}/>:null}*/}
        {/*{props.image?<Figure image={props.image}/>:null}*/}

    </React.Fragment>
}