import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Link from "@material-ui/core/Link";
import LinkIcon from "react-feather/dist/icons/link";

export default function Text(props){
    return <Card>
        <CardContent>
            {props.text}
        </CardContent>
    </Card>
}