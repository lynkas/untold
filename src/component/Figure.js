import React from "react";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";

export default function Figure(props){

    return <Card style={{width:"100%"}}>
        {console.log(props.image)}
        <img src={props.image} style={{width:"100%"}} />
        {/*<CardMedia image={props.image} />*/}
    </Card>
}