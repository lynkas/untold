import React from "react";
import IconButton from "@material-ui/core/IconButton";
import {ArrowBack} from "@material-ui/icons";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import Link from "@material-ui/core/Link";
import Button from "@material-ui/core/Button";
export default function Header(){
    return <div style={{marginTop:"15px",marginBottom:"15px",padding:"10px"}}>
        {/*{props.func!=="/"?<IconButton onClick={()=>{window.location="/"}}>*/}
        {/*    <ArrowBack/>*/}
        {/*</IconButton>:null}*/}

        <Button onClick={()=>{window.location="/"}}>
            <Typography variant={"h4"} display={"inline"} style={{verticalAlign:"middle"}}>untold</Typography>
        </Button>
    </div>
}