import React, {useEffect, useState} from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {Delete} from "@material-ui/icons";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { Base64 }  from 'js-base64'
import {download} from "./utils/utils";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import '../css/distance.scss'
import { useTranslation } from 'react-i18next';

export default function Make(){
    const { t, i18n } = useTranslation();
    const [videos,setVideos]=useState([{}])
    const [fileName,setFileName]=useState("awesome video")
    const [basic,setBasic]=useState({})
    const [result,setResult]=useState("{}")


    const change = (id,item) => e => {
        videos[id][item]=e.target.value
        setVideos([...videos])
    }

    const changeBasic = item => e => {
        basic[item]=e.target.value
        setBasic({...basic})
    }

    const add = e =>setVideos([...videos,{}])

    const remove = id => e => {
        const nVideo = []
        for (const i in videos){
            if (i!=id){
                nVideo.push(videos[i])
            }
        }
        setVideos(nVideo)
    }

    const videoPure = () => {
        const nVideo = []
        for(const i in videos){
            if (((videos[i].title||'')+(videos[i].subtitle||'')+(videos[i].video||'')+(videos[i].image||'')+(videos[i].music||'')+(videos[i].text||'')).trim()!==""){
                nVideo.push(videos[i])
            }
        }
        return nVideo
    }

    const jsonString = ()=>{
        const vp =videoPure()
        const bp = {}
        for (const i in basic){
            if (basic[i]!==undefined&&basic[i].trim()) bp[i]=basic[i]
        }
        const final = vp.length===0?{...bp}:{...bp,series:vp}
        return JSON.stringify(final,null,2)
    }

    useEffect(()=>{
        setResult(jsonString())
    },[videos,basic])

    const fileNameChange = e=>{
        setFileName(e.target.value)
    }

    return <div>

        <Typography variant={"h5"} className={"distance"}> {t("create video config")}</Typography>
        <div className={"distance"}>
            <TextField variant={"filled"} value={basic.title||""} fullWidth label={t("title")} onChange={changeBasic("title")}/>
            <TextField variant={"filled"} value={basic.cover||""} fullWidth label={t("cover link")} onChange={changeBasic("cover")}/>
            <TextField variant={"filled"} value={basic.introduction||""} fullWidth multiline label={t("introduction")} onChange={changeBasic("introduction")}/>
        </div>


        {videos.map((e,i)=>{
            return <div className={"distance"}>
                <div style={{marginBottom:"5px"}}>
                    <Typography variant={"h6"} style={{display:"inline",verticalAlign:"middle"}} >Part {i}</Typography>

                    <Button variant={"outlined"} style={{height:"100%", marginLeft:"10px"}} onClick={remove(i)}>
                        <Delete/>
                    </Button>
                </div >

                <TextField  variant={"filled"} value={videos[i].title||""} placeholder={t("no")+t("title")} fullWidth label={t("part title")} onChange={change(i,"title")}/>
                <TextField  variant={"filled"} value={videos[i].video||""} placeholder={t("no")+t("video")}  fullWidth label={t("video")+t("link")} onChange={change(i,"video")}/>
                <TextField  variant={"filled"} value={videos[i].subtitle||""} placeholder={t("no")+t("subtitle")}  fullWidth label={t("subtitle")+t("link")} onChange={change(i,"subtitle")}/>
                <TextField  variant={"filled"} value={videos[i].music||""} placeholder={t("no")+t("music")}  fullWidth label={t("music")+t("link")} onChange={change(i,"music")}/>
                <TextField  variant={"filled"} value={videos[i].image||""} placeholder={t("no")+t("image")}  fullWidth label={t("image")+t("link")} onChange={change(i,"image")}/>
                <TextField  variant={"filled"} value={videos[i].text||""} placeholder={t("no")+t("text")} multiline fullWidth label={t("text")} onChange={change(i,"text")}/>
            </div>
        })}
        <div className={"distance"}>
            <Button
                size={"large"}
                color={"primary"}
                variant={"outlined"}
                fullWidth
                onClick={add}
                style={{textTransform:"none"}}
                className={"distance"}
            >+</Button>
        </div>


            <ButtonGroup size="large" color="primary" aria-label="result" className={"distance"}>
                <TextField variant={"filled"} label={t("filename")} onChange={fileNameChange} />
                <Button onClick={download(fileName,result)}>
                    {t("save")}
                </Button>
                <Button color={"secondary"} onClick={()=>{window.location=window.location}}>
                    {t("reset")}
                </Button>
            </ButtonGroup>
        <TextField multiline fullWidth variant={"filled"} label={t("result")} value={result} disabled={true}/>

    </div>
}