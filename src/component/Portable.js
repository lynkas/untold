import React from "react";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import {download, load} from "./utils/utils";
import '../css/distance.scss'
import { useTranslation } from 'react-i18next';


export default function Portable(props){
    const { t, i18n } = useTranslation();

    const exp = cate => e =>{
        download()
    }

    const imp = cate => e =>{
        try {
            load((content)=>{
                console.log(content)
                props.operation(cate+"_ADD",content)
            })
        }catch (e) {
            console.log(e)
        }
    }

    const reset = cate => ()=>props.operation(cate+"_RESET")

    return <div>
        <ButtonGroup className={"distance"}>
            <Button color={"primary"} onClick={download("subscribe",JSON.stringify(props.subsribe,null,2))}>
                {t("export")} {t("subscribe")}
            </Button>
            <Button color={"primary"} onClick={download("videolist",JSON.stringify(props.video,null,2))}>
                {t("export")} {t("video")}{t("list")}
            </Button>
        </ButtonGroup>
        <ButtonGroup className={"distance"}>
            <Button color={"primary"} onClick={imp("S")}>
                {t("import")} {t("subscribe")}

            </Button>
            <Button color={"primary"} onClick={imp("V")}>
                {t("import")} {t("video")}{t("list")}
            </Button>
        </ButtonGroup>
        <ButtonGroup className={"distance"}>
            <Button color={"secondary"} onClick={reset("S")}>
                {t("clear")} {t("subscribe")}
            </Button>
            <Button color={"secondary"} onClick={reset("V")}>
                {t("clear")} {t("video")}{t("list")}
            </Button>
        </ButtonGroup>

    </div>
}
