import PropTypes, {func} from 'prop-types';
import React, {useEffect, useReducer, useState} from "react";
import SubscribeListData from "./SubscribeListData";
import VideoListData from "./VideoListData";
import SubscribeList from "./SubscribeList";
import VideoList from "./VideoList";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import {NoteAdd, Settings, Update} from "@material-ui/icons";
import axios from "axios";
import {getAndSaveVideo, inList, originalGet, slHandle} from "./utils/utils";
import {getDB} from "./db/db";
import {LINK, SUBLINK, SUBSCRIBELIST, UPDATE, VIDEO, VIDEOLIST} from "./utils/names";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import SubscriptionsIcon from '@material-ui/icons/Subscriptions';
import {getVideoCursor} from "./utils/db";
export default function Main(props) {

    const [hasRequests,setHasRequests] = useState(false)
    const [last,setLast] = useState(0)
    const [range,setRange] = useState(0)
    const [vidList,setVidList] = useState([])
    const [testList,setTestList]=useState({})
    async function updateSubscribes() {
        if (props.subscriptionList.length!==0){
            setHasRequests(true)
            let transaction = getDB().transaction([SUBSCRIBELIST,VIDEOLIST],"readwrite")
            let subStore = transaction.objectStore(SUBSCRIBELIST)
            let vidStore = transaction.objectStore(VIDEOLIST)
            let request = subStore.index(UPDATE).getAll(1)
            request.onsuccess=e=>{
                if (e.target.result){
                    const all = e.target.result
                    for (const e of all){
                        getAndSaveVideo()
                    }
                }
            }
            const cursor = await cursorSubscribe(subStore)
            while (cursor){
                const [obj,links] =  await slProcess(cursor.value)
                await cursor.update(obj)
                for (const link of links){
                    // await videoLinkProcess(link,obj.link,vidStore)
                }
                cursor.continue()
            }
            setHasRequests(false)

        }
    }

    const cursorSubscribe = async (store)=>{

        let request = await store.openCursor(IDBKeyRange.lowerBound(0),"prev")
        return await new Promise((resolve,reject)=>{

            request.onerror=e=>reject(e)
        })
    }



    const slProcess = async (subscribeObject) => {
        try {
            const response = await originalGet(subscribeObject.link)
            const [introduction,links] = slHandle(response.data)
            subscribeObject["introduction"]=introduction
            subscribeObject["LUT"]=Date.now()

            return [subscribeObject,links]


        }catch (e) {
            console.log(e)
        }
    }


    useEffect(()=>{

        (async ()=>{
            try{
                const result = []
                let temp_last = last
                let cursor = await getVideoCursor()
                try {
                    while (cursor){
                        console.log(cursor)
                        temp_last++
                        result.push(cursor.value)
                        if (result.length<25) cursor=await cursor.continue();
                    }
                }catch (e) {
                    console.log(e)
                }

                    setVidList(result)
                    setLast(temp_last)
            }catch (e) {
                console.log(e)
            }

        })()
    },[range])



    const getCount = async (store) =>{
        const result = await store.count()
        return new Promise((resolve,reject)=>{
            result.onsuccess=(e)=>{resolve(result.result)}
            result.onerror=(e)=>{reject(e)}
        })
    }

    return <Container maxWidth="md">
        <IconButton onClick={()=>{window.location="/sub"}} disabled={null}>
            <SubscriptionsIcon/>
        </IconButton>
        <IconButton onClick={updateSubscribes} disabled={hasRequests}>
            <Update/>
        </IconButton>
        <IconButton onClick={()=>{window.location="/set/make"}} disabled={null}>
            <NoteAdd/>
        </IconButton>
        {/*<IconButton onClick={()=>{window.location="/set/setting"}} disabled={null}>*/}
        {/*    <Settings/>*/}
        {/*</IconButton>*/}

        <VideoList vidList={vidList}/>

        </Container>
}

Main.prototype = {
    subscriptionList: PropTypes.arrayOf(PropTypes.string),
    videoList:PropTypes.arrayOf(VideoListData),
    removeSubscription: PropTypes.func,
    addSubscription: PropTypes.func,
    setVideoList: PropTypes.func,
}