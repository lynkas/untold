import PropTypes from 'prop-types';
import VideoListData from "./VideoListData";
import List from "@material-ui/core/List";
import React, {useEffect, useState} from "react";
import ListItem from "@material-ui/core/ListItem";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActionArea from "@material-ui/core/CardActionArea";
import Container from "@material-ui/core/Container";
import { useTranslation } from 'react-i18next';
import {getDB} from "./db/db";
import {VIDEOLIST} from "./utils/names";


export default function VideoList(props){
    const { t, i18n } = useTranslation();


    // const getCursor = async (store,direction="prev") =>{
    //     const request =await store.openCursor(null,direction)
    //     return new Promise((resolve,reject)=>{
    //         request.onsuccess=(e)=>{
    //             console.log(e)
    //             console.log(request)
    //             resolve(request.result)}
    //         request.onerror=(e)=>{reject(e)}
    //     })
    // }

    return <List>
        {props.vidList.map((e)=>{
            return <ListItem className={"border-none"} style={{paddingLeft:0,paddingRight:0,}}>
                <Card style={{width:"100%"}}>
                    <CardActionArea onClick={()=>{
                        window.location="/vid/"+e.link
                        // props.gotoVideo(e.url)
                    }}
                    style={{display:"flex",justifyContent:"flex-start"}}
                    >
                        {e.cover?<CardMedia
                            style={{maxWidth:"200px",maxHeight:"200px"}}
                            component={"img"}
                            image={e.cover}
                            title={e.title}
                        />:null}

                        <CardContent>
                            <Typography variant={"h5"} >
                                {e.title}
                            </Typography>
                            <Typography variant={"subtitle1"} >
                                {console.log(e.time)}
                                {(e.hasOwnProperty("time")&&e.time>0)||(e.part!=null&&e.part>1)?t("watched")+" P"+((e.part!=null&&e.part>1)?e.part:"")+", "+Math.floor(e.time/60)+" "+t("min"):t("never watched")}
                            </Typography>
                            <Typography variant={"subtitle1"} >
                                {e.hasOwnProperty("series")&&e.series.length>0?e.series.length+"P":t("no videos")}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
            </ListItem>
        })}
    </List>
}


VideoList.prototype={
    videoList:PropTypes.arrayOf(VideoListData),
    // gotoVideo:PropTypes.func
}

