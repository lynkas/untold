import PropTypes from 'prop-types';
import React, {useEffect, useRef, useState} from "react";
import axios from 'axios'
import { Base64 }  from 'js-base64'
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {Subtitles} from "@material-ui/icons";

export default function Video(props) {
    const [subtitleBase64,setSubtitleBase64]=useState("")

    const ref=useRef(null);

    useEffect(()=>{
        if (props.hasOwnProperty("subtitle")&&props.subtitle){
            console.log(props.subtitle)
            axios.get(props.subtitle).then((response)=>{
                const b64=Base64.encode(response.data)
                setSubtitleBase64("data:text/vtt;base64,"+b64)
            }).catch((e)=>{
                console.log(e)
            })
        }

    },[])

    useEffect(()=>{
        try {
            if(ref.current.textTracks.length>0){
                ref.current.textTracks[0].mode="showing"
            }
        }catch (e) {
            console.log(e)
        }
    },[subtitleBase64])

    useEffect(()=>{

        let interval = setInterval(()=>{
            if (ref!=null&&!ref.current.paused) {
                props.setTime(ref.current.currentTime);
            }
        },5000)
        return ()=>{clearInterval(interval)}
    },[])

    useEffect(()=> {
        if (ref.current!=null){
            ref.current.currentTime=props.time.toString()
        }
    },[props.time])


    return <div>

        <video ref={ref} key={props.video} controls style={{width:"100%"}}>
            <source src={props.video} />
            {subtitleBase64!==""?<track src={subtitleBase64} label={"caption"}/>:null}
        </video>
    </div>
}

Video.prototype={
    videoUrl:PropTypes.string.isRequired,
    subtitleUrl:PropTypes.string,
    time:PropTypes.number.isRequired,
    setTime:PropTypes.func

}


