import React from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import {IMAGE, LINK, MUSIC, VIDEO} from "./utils/names";
import {ExternalLink, Film, Image, Music} from "react-feather";
import {currentSeries} from "./utils/utils";


export default function Introduction(props) {

    const icon = seriesObject => {

        const icons=[]
        for (const i in seriesObject){

            switch (i) {
                case LINK:
                    if(seriesObject[i]) icons.push(<ExternalLink />)
                    break
                case VIDEO:
                    if(seriesObject[i]) icons.push(<Film />)
                    break
                case MUSIC:
                    if(seriesObject[i]) icons.push(<Music />)
                    break
                case IMAGE:
                    if(seriesObject[i]) icons.push(<Image />)
                    break
            }
        }

        return icons
    }

    return <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
            <Typography variant={"h4"}>{props.title}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{display: "block",overflow:"hidden"}}>
            <Typography variant={"body2"} style={{marginBottom:"10px"}}>
                {icon(currentSeries(props))}
            </Typography>
            <Typography variant={"subtitle1"}>{props.introduction}</Typography>
        </ExpansionPanelDetails>
    </ExpansionPanel>

}