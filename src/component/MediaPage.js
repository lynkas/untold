import PropTypes, {func} from 'prop-types';
import React, {useEffect, useRef, useState} from "react";
import Video from "./Video";
import SeriesList from "./SeriesList";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {Link, Subtitles} from "@material-ui/icons";
import Grid from "@material-ui/core/Grid";
import "../css/icons.scss"
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import {currentSeries, getLink, getOneVideo, one, validURL} from "./utils/utils";
import {getDB} from "./db/db";
import {LINK, VIDEO, VIDEOLIST} from "./utils/names";
import axios from "axios";
import {addVideoToDB, getVideoLinkFromDB} from "./utils/db";
import Media from "./Media";
import Introduction from "./Introduction";
import {Router} from "react-router";

export default function MediaPage(props) {

    const [configUrl,setConfigUrl] = useState(()=>{
        return getLink(window.location)
    })
    const [pageData,setPageData] = useState({})

    useEffect(()=>{
        if (configUrl===null)return
        if (!validURL(configUrl))return
        (async ()=>{
            try{
                const value = await getVideoLinkFromDB(configUrl)
                console.log(value)
                if (value)setPageData(value)
                else {
                    const data = await getOneVideo(configUrl)
                    if (data){
                        addVideoToDB(data,configUrl)
                        setPageData(data)
                    }
                }
            }catch (e) {
                console.log(e)
            }

        })()


    },[configUrl])

    //set title
    useEffect(()=>{
        if (pageData.title)
            document.title=pageData.title+" | untold"
    },[pageData])

    //change part
    const change = part => e =>{
        setPageData({...pageData,part})
    }

    useEffect(()=>{
        if (pageData.link){
            addVideoToDB(pageData,configUrl)
        }

    },[pageData])


    const setTime = (t)=>{
        const o = {...pageData,time:t}
        getDB().transaction([VIDEOLIST],"readwrite").objectStore(VIDEOLIST).put(o)
    }

    const setPart = (p) => {
        const o = {...pageData,time:0,part:p}
        setPageData(o)
        getDB().transaction([VIDEOLIST],"readwrite").objectStore(VIDEOLIST).put(o)
    }

    return <div>
        {!one(pageData)?"fail or loading":  //TODO
            <React.Fragment>
                <Grid  spacing={5}>
                    <Grid item lg={8} sm={12} xs={12}>
                        <Media {...currentSeries(pageData)} time={pageData.time?pageData.time:0} setTime={setTime}/>
                    </Grid>
                    <Grid item lg={4} sm={12} xs={12}>
                        <Introduction {...pageData}/>
                        {pageData.series&&pageData.series.length?<SeriesList playingPart={pageData.part} setPart={setPart} series={pageData.series}/>:null}
                    </Grid>
                </Grid>
            </React.Fragment>
        }
    </div>
}

MediaPage.prototype = {
    title: PropTypes.string,
    url: PropTypes.string,
    introduction: PropTypes.string,
    series: PropTypes.array,
    part: PropTypes.number,
    time: PropTypes.number,
    setTime: PropTypes.func
}
