import React, {useEffect, useState} from "react";
import {getLink, originalGet, slHandle, getConfig, getOneSubscribe, getOneVideo} from "./utils/utils";
import {getDB} from "./db/db";
import {HIDE, INTRODUCTION, LINK, LOADED, NAME, SUBLINK, SUBSCRIBELIST, UPDATE, VIDEOLIST} from "./utils/names";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import axios from "axios"
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {
    addOneSubscribeToDB,
    addVideoToDB,
    addVideoToDBNoReplace, deleteSubscribeFromDB,
    getSubscribeLinkFromDB,
    getVideoBySubscribeLink
} from "./utils/db";
import VideoList from "./VideoList";
import Container from "@material-ui/core/Container";


export default function Subscribe(props) {
    const [link,setLink] = useState(()=>{
        return getLink(window.location)
    })
    const [exist,setExist] = useState(null)
    const [intro,setIntro] = useState("")
    const [name,setName] = useState("")
    const [links,setLinks] = useState([])

    const [hp,setHP] = useState(false)
    const [du,setDU] = useState(false)
    const [loadError,setLoadError] = useState(null)
    const [vidList,setVidList] = useState([])
    const getInfo = async link =>{
        try{
            const [introduction,links] = await getOneSubscribe(link)
            console.log(links)
            setIntro(introduction)
            setLinks(links)
            setLoadError(false)
        }catch (e) {
            setLoadError(true)
        }
    }


    useEffect(()=>{
        if (!link)return
        (async ()=>{
            try{
                await getInfo(link)
                const value = await getSubscribeLinkFromDB(link)
                setExist(!!value)
            }catch (e) {
                console.log(e)
            }
        })()

    },[link])

    const save = async () =>{
        await addOneSubscribeToDB(link,intro,!du?1:0)
        if (hp){
            for (const l of links){
                try{
                    await addVideoToDB({},l,link,false)
                }catch (e) {
                    console.log(e)
                }
            }
        }else {
            for (const l of links){
                try{
                    const data= await getOneVideo(l)
                    await addVideoToDB(data,l,link,true)
                }catch (e) {
                    console.log(e)
                }
            }
        }
        window.location=window.location
    }



    useEffect(()=>{
        (async ()=>{
            const videos = await getVideoBySubscribeLink(link)
            if (videos&&videos.length)setVidList(videos)
            else {
                if (links){
                    for (const l of links){
                        console.log(l)
                        try {
                            const v=await getOneVideo(l)
                            console.log(v)
                            if (v){
                                v.link=l
                                setVidList((s)=>{setVidList([...s,v])})
                            }
                        }catch (e) {
                            console.log(e)
                        }
                    }

                }
            }
        })()
    },[links])

    const remove = ()=>{
        deleteSubscribeFromDB(link)
        window.location=window.location
    }

    return <Container maxWidth={"md"}>

        {exist!==null?(<div>
                    {exist?"record exists":"add new record"}
                <TextField variant={"filled"} label={NAME} placeholder={"(blank)"} fullWidth onChange={e=>{setName(e.target.value)}} value={name}/>
                <hr />
                <TextField variant={"filled"} label={LINK} value={link} fullWidth disabled={true}/>
                    <TextField multiline variant={"filled"} label={INTRODUCTION} disabled={true} fullWidth placeholder={"(blank)"} value={intro}/>
                    <TextField multiline variant={"filled"} label={"media count"} disabled={true} fullWidth value={links.length}/>
                {exist?<Button color={"Secondary"} variant={"outlined"} size={"large"} onClick={remove}>remove</Button>:<div>
                    <FormControlLabel
                        control={<Checkbox checked={hp} onChange={()=>{setHP(!hp)}} />}
                        label="hide previous videos"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={du} onChange={()=>{setDU(!du)}} />}
                        label="do not update the subscription"
                    />
                    {loadError===null?"loading":
                        (loadError===false?<Button color={"primary"} variant={"outlined"} size={"large"} onClick={save}>do</Button>:"load error")
                    }
                </div>
                }

            </div>
            ):"querying"}

            <VideoList vidList={vidList}/>

    </Container>

}