import React from "react";

export default function Audio (props){

    return <audio key={props.music}>
        <source src={props.music}/>
    </audio>
}