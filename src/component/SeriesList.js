import PropTypes from 'prop-types';
import React, {useEffect, useRef, useState} from "react";
import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {PlayArrow, Theaters} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import { useTranslation } from 'react-i18next';
import {IMAGE, LINK, MUSIC, VIDEO} from "./utils/names";
import {ExternalLink, FileText, Film, Image, Music} from "react-feather";


export default function SeriesList(props) {
    const { t, i18n } = useTranslation();



    return <List subheader={<ListSubheader>{t("media")}</ListSubheader>}>
        {props.series.map((e,i)=>
            <React.Fragment  key={JSON.stringify(e)}>
                <Divider />
                <ListItem
                    selected={props.playingPart===i}
                    onClick={()=>{props.setPart(i)}}

                >
                    <ListItemIcon>
                        {props.playingPart===i?<PlayArrow/>:<div style={{height:"24px",width:"24px",textAlign:"center"}}>{i+1}</div>}
                    </ListItemIcon>
                    <ListItemText primary={e.hasOwnProperty("title")&&e.title?e.title:t("(blank)")} />
                </ListItem>
            </React.Fragment>
        )}
    </List>
}

SeriesList.prototype={
    playingPart:PropTypes.number,
    series:PropTypes.array,
    changePart:PropTypes.func
}